using System;
using Xunit;
using Matrices.Models;

namespace Matrices.Tests
{
    public class UnitTest1
    {

        [Fact]
        public void TestSquareBracketOperator()
        {
            var arr1 = new double[] {1, 2, 3, 4};
            var m1 = new Matrix(2,2,arr1);
            var m1_00 = m1[0,0];
            var m1_00_shouldbe = 1;
            var m1_01 = m1[0,1];
            var m1_01_shouldbe = 2;
            var m1_10 = m1[1,0];
            var m1_10_shouldbe = 3;
            var m1_11 = m1[1, 1];
            var m1_11_shouldbe = 4;
            Assert.True(m1_00 == m1_00_shouldbe);
            Assert.True(m1_01 == m1_01_shouldbe);
            Assert.True(m1_10 == m1_10_shouldbe);
            Assert.True(m1_11 == m1_11_shouldbe);
        }

        [Fact]
        public void TestSquareBracketOperator2()
        {
            var arr1 = new double[] {1, 2, 3, 4, 5, 6};
            var m1 = new Matrix(2, 3, arr1);
            var m1_02 = m1[0,2];
            var m1_02_shouldbe = 3;
            var m1_10 = m1[1,0];
            var m1_10_shouldbe = 4;
            var m1_12 = m1[1,2];
            var m1_12_shouldbe = 6;
            Assert.True(m1_02 == m1_02_shouldbe);
            Assert.True(m1_10 == m1_10_shouldbe);
            Assert.True(m1_12 == m1_12_shouldbe);
        }


        [Fact]
        public void TestMatrixMultpiplication()
        {
            var m1 = new Matrix(2, 2, new double[]{1, 1, 1, 1});
            var m2 = new Matrix(2, 2, new double[]{1, 1, 1, 2});
            var m3 = m1 * m2;
            var shouldBe = new Matrix(2, 2, new double[]{2, 3, 2, 3});
            Assert.True(m3 == shouldBe);
        }

         [Fact]
        public void TestMatrixMultpiplication2()
        {
            var m1 = new Matrix(2, 3, new double[]{1, 1, 1, 1, 2, 2});
            var m2 = new Matrix(3, 3, new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9});
            var m3 = m1 * m2;
            var shouldBe = new Matrix(2, 3, new double[]{12, 15, 18, 23, 28, 33});
            //Console.WriteLine(m3);
            Assert.True(m3 == shouldBe);
        }

        [Fact]
        public void TestMatrixMultpiplication3()
        {
            var m1 = new Matrix(3, 2, new double[]{1, 1, 1, 1, 2, 2});
            var m2 = new Matrix(2, 3, new double[]{1, 2, 3, 4, 5, 6});
            var m3 = m1 * m2;
            var shouldBe = new Matrix(3, 3, new double[]{5, 7, 9, 5, 7, 9, 10, 14, 18});
            // Console.WriteLine(m3);
            Assert.True(m3 == shouldBe);
        }

         [Fact]
        public void TestMatrixMultpiplication4()
        {
            var m1 = new Matrix(1, 1, new double[]{1});
            var m2 = new Matrix(1, 1, new double[]{1});
            var m3 = m1 * m2;
            var shouldBe = new Matrix(1, 1, new double[]{1});
            Assert.True(m3 == shouldBe);
        }

        [Fact]
        public void TestMatrixAddition()
        {
            var m1 = new Matrix(2, 2, new double[]{1, 1, 1, 1});
            var m2 = new Matrix(2, 2, new double[]{1, 1, 1, 2});
            var m3 = m1 + m2;
            var shouldBe = new Matrix(2, 2, new double[]{2, 2, 2, 3});
            Assert.True(m3 == shouldBe);
        }

         [Fact]
        public void TestMatrixAddition2()
        {
            var m1 = new Matrix(1, 1, new double[]{2});
            var m2 = new Matrix(1, 1, new double[]{2});
            var m3 = m1 + m2;
            var shouldBe = new Matrix(1, 1, new double[]{4});
            Assert.True(m3 == shouldBe);
        }

        [Fact]
        public void TestMatrixTranspose()
        {
            var m1 = new Matrix(2, 2, new double[]{1, 2, 3, 4});
            var shouldBe = new Matrix(2, 2, new double[]{1, 3, 2, 4});
            var mTransposed = m1.Transpose();
            Assert.True(mTransposed == shouldBe);
        }

        [Fact]
        public void TestMatrixTranspose2()
        {
            var m1 = new Matrix(2, 3, new double[]{1,2,3,4,5,6});
            var shouldBe = new Matrix(3, 2, new double[]{1, 4, 2, 5, 3, 6});
            var mTransposed = m1.Transpose();
            Assert.True(mTransposed == shouldBe);
        }

        [Fact]
        public void TestMatrixTranspose3()
        {
            var m1 = new Matrix(5, 2, new double[]{1, 2 , 3, 4, 5, 6, 7, 8, 9, 10});
            var shouldBe = new Matrix(2, 5, new double[]{1, 3, 5, 7, 9, 2, 4, 6, 8, 10});
            var mTransposed = m1.Transpose();
            Assert.True(mTransposed == shouldBe);
        }
    }
}
