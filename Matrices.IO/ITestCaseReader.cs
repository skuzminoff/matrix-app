using System.Collections.Generic;
using Matrices.Models;

namespace Matrices.IO
{
     public interface ITestCaseReader 
    {
        IEnumerable<TestCase> Read();
    }
}