using System.IO;
using Matrices.Models;

namespace Matrices.IO
{
    public class ResultWriter : ITestCaseResultWriter
    {

        string _directoryPath;

        public ResultWriter(string directoryPath)
        {
            _directoryPath = directoryPath;
        }

        public void Write(ITestCaseResult tcr, string testCaseName)
        {
            string resultName =  testCaseName + "_result.txt";
            string fullPath = Path.Combine(_directoryPath, resultName);

            File.WriteAllText(fullPath, tcr.ToString());
        }
    }
}