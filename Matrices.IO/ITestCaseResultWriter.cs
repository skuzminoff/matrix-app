using Matrices.Models;

namespace Matrices.IO
{
    public interface ITestCaseResultWriter
    {
        void Write(ITestCaseResult tcResult, string testCaseName);
    }

}