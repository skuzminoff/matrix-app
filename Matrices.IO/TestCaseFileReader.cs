﻿using System;
using System.IO;
using Matrices.Models;
using System.Collections.Generic;
using System.Linq;

namespace Matrices.IO
{
    public class TestCaseFileReader : ITestCaseReader
    {
        string _diretoryPath;

        public TestCaseFileReader(string directoryPath)
        {
            _diretoryPath = directoryPath;
        }
        public IEnumerable<TestCase> Read()
        {
            var examplesDir = new DirectoryInfo(_diretoryPath);
            if (!examplesDir.Exists)
               throw new Exception("Directory does not exist");
            var tcList = new List<TestCase>();
            foreach (var fi in examplesDir.GetFiles())
            {
                string data = "";
                using(var fs = new StreamReader(fi.FullName))
                    data = fs.ReadToEnd();
                
                try
                {
                    (Operation op, IEnumerable<Matrix> matrices) = _readTestCaseData(data);
                    var testCaseName = Path.GetFileNameWithoutExtension(fi.Name);
                    var tc = new TestCase(matrices, op, testCaseName);
                    tcList.Add(tc);
                }
                catch 
                {
                    Console.WriteLine($"Error while reading file {fi.FullName}");
                }
            }
            return tcList;
        }

        private (Operation, IEnumerable<Matrix>) _readTestCaseData(string data)
        {
            var listStrings = data.Split(new string[]{"\n\n"}, StringSplitOptions.None);
            var op = listStrings[0];
            var matrices = new List<Matrix>();
            var a = new List<double>();
            foreach (var str in listStrings.Skip(1))
            {
                var rows = str.Split('\n');
                var cols = rows[0].Trim().Count(ch => ch == ' ') + 1;
                foreach (var row in rows)
                {
                    var b = row.Split(' ');
                    a.AddRange(b.Select(item => Convert.ToDouble(item)));
                }
                var c = new Matrix(rows.Length, cols, a.ToArray());
                matrices.Add(c);
                a.Clear();
            }
            //return new TestCase(matrices, _getOperationFromString(op));
            return (_getOperationFromString(op), matrices);
        }

          private Operation _getOperationFromString(string op)
        {
            if (!String.IsNullOrEmpty(op))
            {
                switch(op)
                {
                    case "multiply":
                        return Operation.Multiply;
                    case "add":
                        return Operation.Add;
                    case "subtract":
                        return Operation.Subtract;
                    case "transpose":
                        return Operation.Transpose;
                }
            }
            throw new Exception("Invalid operation token");
        }
    }
}
