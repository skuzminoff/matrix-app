﻿using System;
using System.IO;
using Matrices.IO;
using Matrices.TestCaseExecutor;

namespace Matrices.UI
{
    class Program
    {



        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                return;
            }

            var examplesDirPath = args[0];
            var tcReader = new TestCaseFileReader(examplesDirPath);
            var tcList = tcReader.Read();
            var tcResultWriter = new ResultWriter(examplesDirPath);
            var tcExecutor = new TestCaseExecutor.TestCaseExecutor();
            foreach(var el in tcList)
            {
                Console.WriteLine($"Start execution of {el.TestCaseName}");
                try
                {
                    var result = tcExecutor.Execute(el);
                    tcResultWriter.Write(result,el.TestCaseName);
                    Console.WriteLine($"Execution of {el.TestCaseName} completed");
                }
                catch
                {
                    Console.WriteLine($"Error while executing testcase {el.TestCaseName}");
                }
            }
        }
    }
}
