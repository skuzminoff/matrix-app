using System.Collections.Generic;
using System;
using System.Linq;

namespace Matrices.Models {

    public enum Operation {
        Multiply,
        Add,
        Subtract,
        Transpose
    }

    public class TestCase 
    {
        public IEnumerable<Matrix> Matrices {get; private set;}
        public Operation Op {get; private set;}
        
        public string TestCaseName {get; private set;}

        public TestCase (IEnumerable<Matrix> matrices, Operation op, string testCaseName) 
        {
            Matrices = matrices;
            Op = op;
            TestCaseName = testCaseName;
        }

        public override string ToString()
        {
            var name = ((Operation)Op).ToString();
            string s = name + Environment.NewLine;

            foreach (var m in Matrices)
            {
                s += m.ToString();
                s += Environment.NewLine;
            }
            return s;
        }

    }
}