using System;

namespace Matrices.Models
{
    public interface ITestCaseResult : IFormattable {
        object Result { get; }
    }
}