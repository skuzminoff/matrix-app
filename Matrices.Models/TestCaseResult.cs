using System;
using System.Collections.Generic;

namespace Matrices.Models {

    public class TestCaseMatrixResult : ITestCaseResult {
        public object Result { get; private set; }

        public TestCaseMatrixResult (Matrix result) {
            Result = result;
        }

        public string ToString (string format, IFormatProvider formatProvider) {
            return Result.ToString ();
        }

        public override string ToString()
        {
            return ToString(null, System.Globalization.CultureInfo.CurrentCulture);
        }
    }

    public class TestClassMultipleMatrixResult : ITestCaseResult {
        public object Result { get; private set; }

        public TestClassMultipleMatrixResult (IEnumerable<Matrix> result) {
            Result = result;
        }

        public string ToString (string format, IFormatProvider formatProvider) {
            var mcs = (IEnumerable<Matrix>) Result;
            string s = "";
            foreach (var m in mcs) {
                s += m.ToString ();
                s += Environment.NewLine;
            }
            return s;
        }

        public override string ToString()
        {
            return ToString(null, System.Globalization.CultureInfo.CurrentCulture);
        }

    }
}