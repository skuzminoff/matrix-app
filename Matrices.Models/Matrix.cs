﻿using System;

namespace Matrices.Models
{
    public class Matrix
    {
        int _rows;
        int _cols;
        double[] _sarr;

        public Matrix (int rows, int cols, double[] sarr)
        {
            _rows = rows;
            _cols = cols;
            _sarr = sarr;
        }

       public double[] _getRow(int i)
       {
           if (i >= _rows)
           {
                throw new Exception("Wrong dimension");
           }
           var row = new double[_cols];
           for (int j=0;j<_cols;j++)
           {
               row[j] = this[i,j];
           }
           return row;
       }

       public double[] _getCol(int j)
       {
           if (j >= _cols)
           {
                throw new Exception("Wrong dimension");
           }
           var col = new double[_rows];
           for (int i=0;i<_rows;i++)
           {
               col[i] = this[i, j];
           }
           return col;
       }

        public double this[int i, int j]
        {
            get => _sarr[i * _cols + j];
            set => _sarr[i * _cols + j] = value;
        }

        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            if (m1._cols != m2._rows)
            {
                throw new Exception("Dimensions incompatible");
            }

            double scalarMultArray(double[] arr1, double[] arr2)
            {
                double prod = 0;
                for (int i=0; i<arr1.Length; i++)
                    prod += arr1[i] * arr2[i];
                return prod;
            }

            var arr =  new double[m1._rows * m2._cols];
            for(int i=0; i<m1._rows; i++)
            {
                for (int j=0; j<m2._cols; j++)
                {
                    arr[i*m2._cols + j] = scalarMultArray(m1._getRow(i), m2._getCol(j));
                }
            }
            return new Matrix(m1._rows, m2._cols, arr);
        }

        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            if (m1._rows != m2._rows || m1._cols != m2._cols)
            {
                throw new Exception("Dimensions incompatible");
            }
            int height = m1._rows;
            int width = m1._cols;
            var arr =  new double[height * width];
            for(int i=0; i<height; i++)
            {
                for (int j=0; j<width; j++)
                {
                    arr[i*width + j] = m1[i,j] + m2[i,j];
                }
            }
            return new Matrix(height, width, arr);
        }

        public static Matrix operator -(Matrix m1, Matrix m2)
        {
            if (m1._rows != m2._rows || m1._cols != m2._cols)
            {
                throw new Exception("Dimensions incompatible");
            }
            int height = m1._rows;
            int width = m1._cols;
            var arr =  new double[height * width];
            for(int i=0; i<height; i++)
            {
                for (int j=0; j<width; j++)
                {
                    arr[i*width + j] = m1[i,j] - m2[i,j];
                }
            }
            return new Matrix(height, width, arr);
        }

        public static bool operator==(Matrix m1, Matrix m2)
        {
            if((object)m1 == null)
            {
                if ((object)m2 == null)
                    return true;
                else
                    return false;
            }
            return (m1.Equals(m2));
        }

        public static bool operator!=(Matrix m1, Matrix m2)
        {
            return !(m1 == m2);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            Matrix m = obj as Matrix;
            if (_cols != m._cols || _rows != m._rows)
                return false;
            for (int i=0; i<_rows; i++)
            {
                for (int j=0; j<_cols; j++)
                {
                    if (this[i,j] != m[i,j])
                        return false;
                }
            }
            return true;
        }

        public override int GetHashCode() {
            return _sarr.GetHashCode();
        }

        public  Matrix Transpose()
        {
            var arr = new double[_cols * _rows];
            for  (int i=0; i<_rows;i++)
            {
                for (int j=0; j<_cols;j++)
                {
                    arr[_rows * j + i] = this[i,j];
                }
            }
            return new Matrix(_cols, _rows, arr);
        }

        public override string ToString()
        {
            string s = ($"{_rows}, {_cols}");
            s += Environment.NewLine;
            for (int i=0; i<_rows; i++)
            {
                for (int j=0; j<_cols; j++)
                {
                    s += this[i,j] + " ";
                }
                s += Environment.NewLine;
            }
            return s;
        }

    }
}
