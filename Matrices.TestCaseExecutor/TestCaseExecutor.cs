﻿using System;
using Matrices.Models;
using System.Linq;
using System.Collections.Generic;

namespace Matrices.TestCaseExecutor
{
    public interface ITestCaseExecutor
    {
        ITestCaseResult Execute(TestCase tc);
    }

    public class TestCaseExecutor : ITestCaseExecutor
    {
        public ITestCaseResult Execute(TestCase tc)
        {
            var mcs = tc.Matrices.ToList();
            var m = mcs[0];
            ITestCaseResult result = null;
            switch(tc.Op)
            {
                case Operation.Multiply:
                    for (int i = 1; i< mcs.Count(); i++)
                        m = m * mcs[i];
                    result = new TestCaseMatrixResult(m);
                    break;
                case Operation.Add:
                    for (int i = 1; i< mcs.Count(); i++)
                        m = m + mcs[i];
                    result = new TestCaseMatrixResult(m);
                    break;
                case Operation.Subtract:
                    for (int i = 1; i< mcs.Count(); i++)
                        m = m - mcs[i];
                    result = new TestCaseMatrixResult(m);
                    break;
                case Operation.Transpose:
                    var res = new List<Matrix>();
                    foreach (var mx in mcs)
                        res.Add(mx.Transpose());
                    result = new TestClassMultipleMatrixResult(res);
                    break;
            }
            return result;
        }
    }
}
